---
layout: page
title: pub
permalink: /pub/
---
<ol>

<p>2015</p>

<li> <a href="{{site.baseurl}}/pb/qc/SLL1.pdf">SLL1</a>: What is a Quasicrystal? Mathematical Foundations of Quasicrystallography

   <br><a href="{{site.baseurl}}/pb/qc/SLL1_P.pdf">SLL1_P</a>: Presentation: 'What is a Quasicrystal? Mathematical Foundations of Quasicrystallography'  </li><br>

<li> <a href="{{site.baseurl}}/pb/rmtrh/SLL2.pdf">SLL2</a>: Architecture of a Nucleus: Nuclear Spectra and Random Matrix Theory</li><br>

<li> <a href="{{site.baseurl}}/pb/quach/SLL3.pdf">SLL3</a>: Classical Chaos and Quantum Chaology</li> 
<br>
<p>2016</p>

<li> <a href="{{site.baseurl}}/pb/ro/SLL4.pdf">SLL4</a>: Reverse Osmosis and Water Purification: Process and Procedures</li>

</ol>
